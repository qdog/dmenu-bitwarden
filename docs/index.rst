.. Bitwarden Dmenu documentation master file, created by
   sphinx-quickstart on Fri Feb  5 13:56:09 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Bitwarden Dmenu's documentation!
===========================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   cli

   developer


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
