Developer Notes
===============

This document describes how this program works.

When the script is run it locates the session key file, which is expected
to be in `~/.cache/`. If the file does not exist it creates an empty one.
Then it reads the file and attempts to use it to unlock the Bitwarden vault.
If it can not unlock the vault, it asks the user for the password using dmenu
and creates a new session key, which it then saves to the session key file.
Then the user selects the desired password via dmenu, which is then copied
to the clipboard.
