#!/usr/bin/python3
"""lets you choose an account from bitwarden-cli and gives the password to your clipboard"""

import os
import subprocess
import json
from enum import Enum
from typing import Optional

import cli
import vault
import utils

KEY_PATH = f"{os.getenv('HOME')}/.cache/bitwarden_session_key.txt"
COLOR = "#AAA"
DELIM = " - "
sess_storage_impl = utils.KeyctlSessionStorage() #utils.FileSessionStorage(filepath=KEY_PATH)


class ItemsTypes(int, Enum):
    LOGIN = 1
    NOTE = 2
    CARD = 3
    IDENTITY = 4


class WantedValue(str, Enum):
    PASSWORD = 'password'
    USERNAME = 'username'
    NOTE = 'note'


def get_items(item_type: set[ItemsTypes]):
    sess_key = sess_storage_impl.load_session_key()

    try:
        # try to use loaded sess key
        f = vault.get_items(sess_key)
    except subprocess.TimeoutExpired:
        sess_key = vault.get_new_key(COLOR)

        # note the new key
        sess_storage_impl.save_session_key(sess_key)

        try:
            f = vault.get_items(sess_key)
        except subprocess.TimeoutExpired:
            utils.notify("Something went wrong")
            exit(1)

    comm_out = f.stdout
    # ignore err mess
    i = comm_out.find("[")
    comm_out = comm_out[i:]
    # parse json content
    json_out = json.loads(comm_out)
    return [item for item in json_out if item["type"] in item_type]


def create_and_get_dmenu_result(wanted_values: set[WantedValue], parsed: list[dict]) -> Optional[dict]:
    # choose with dmenu
    prompt_items = []
    if WantedValue.PASSWORD in wanted_values:
        prompt_items += ["Passwords"]
    if WantedValue.USERNAME in wanted_values:
        prompt_items += ["Usernames"]
    if WantedValue.NOTE in wanted_values:
        prompt_items += ["Notes"]

    prompt = ", ".join(prompt_items) + " :"

    names = "\n".join([f"{acc['name']}{DELIM}{acc['login']['username']}" if acc['type']
                                                                            == 1 else f"{acc['name']}{DELIM}[NOTE]" for
                       acc in parsed])
    g = subprocess.run(["dmenu", "-i", "-p", prompt],
                       input=names.encode("UTF-8"), stdout=subprocess.PIPE, check=True)
    # remove \n
    name, username = g.stdout.decode("UTF-8")[:-1].split(DELIM)
    # find selected item
    for item in parsed:
        if item['name'] == name and (
                (item["type"] == ItemsTypes.LOGIN and item['login']['username'] == username) or
                item["type"] == ItemsTypes.NOTE):
            return item


def get_clip_value(item: dict, wanted_values: set[WantedValue]) -> str:
    if item['type'] == ItemsTypes.LOGIN:
        if WantedValue.USERNAME in wanted_values:
            return item['login']['username']
        elif WantedValue.PASSWORD in wanted_values:
            return item['login']['password']
        else:
            raise AssertionError("this case is not handled")
    elif item['type'] == ItemsTypes.NOTE:
        return item['notes']
    else:
        utils.notify("Unknown match type")
        raise AssertionError("Unknown match type")


if __name__ == "__main__":
    args = cli.create_parser().parse_args()

    # filter logins or notes
    types = set()
    wanted_values = set(args.wanted_values)
    if len(wanted_values) > 1:
        raise AssertionError("this case is not handled yet")

    if WantedValue.PASSWORD in wanted_values or WantedValue.USERNAME in wanted_values:
        types.add(ItemsTypes.LOGIN)
    elif WantedValue.NOTE in wanted_values:
        types.add(ItemsTypes.NOTE)
    else:
        raise AssertionError("did not expect this argument")

    parsed = get_items(item_type=types)

    final_match = create_and_get_dmenu_result(wanted_values=wanted_values, parsed=parsed)

    assert final_match is not None, "Your query was not found. Something went wrong. Please inspect the code or start a Github issue."

    inp = get_clip_value(item=final_match, wanted_values=wanted_values)

    # copy to clipboard
    subprocess.run(["xclip", "-selection", "clipboard"],
                   input=inp.encode("UTF-8"))
