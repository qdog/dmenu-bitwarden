#!/usr/bin/env python3
"""module to parse the command-line options"""

import argparse
import dmenu_bitwarden

def create_parser():
    """parse command-line options"""
    parser = argparse.ArgumentParser(description='Default behavior is to copy selected password or note content to clipboard')
    # https://stackoverflow.com/a/11155124
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-u', '--username', help="Get username to clipboard instead. (notes are excluded)",
                       const=dmenu_bitwarden.WantedValue.USERNAME, action="append_const", dest="wanted_values")
    group.add_argument('-p', '--password', help='Get passwords to clipboard (notes are excluded)',
                       const=dmenu_bitwarden.WantedValue.PASSWORD, action="append_const", dest="wanted_values")
    group.add_argument('-n', '--notes', help='Get notes content to clipboard (logins are excluded)',
                       const=dmenu_bitwarden.WantedValue.NOTE, action="append_const", dest="wanted_values")
    return parser
