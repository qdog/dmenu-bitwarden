#!/usr/bin/env python3
"""utility functions"""

import subprocess
import os
from abc import ABC, abstractmethod
from typing import Optional, overload, Literal, Union, Any


def notify(text: str) -> None:
    """notify user about an event"""
    subprocess.run(["notify-send", text, ], check=True)


@overload
def run_cmd(args: list[str], check: Literal[True] = True, **kwargs: Any) -> str: ...


@overload
def run_cmd(args: list[str], check: Literal[False], **kwargs: Any) -> tuple[str, str]: ...


def run_cmd(args: list[str], check: bool = True, **kwargs: Any) -> Union[str, tuple[str, str]]:
    g = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=check, **kwargs)
    result = g.stdout.decode("UTF-8")[:-1]
    error = g.stderr.decode("UTF-8")[:-1]
    if len(error) == 0:
        return result
    else:
        return result, error


class SessionStorage(ABC):

    @abstractmethod
    def load_session_key(self) -> str:
        pass

    @abstractmethod
    def save_session_key(self, key: str) -> None:
        pass

    @abstractmethod
    def clear_session_key(self) -> None:
        pass


class FileSessionStorage(SessionStorage):

    def __init__(self, filepath: str):
        self.filepath = filepath

    def load_session_key(self) -> str:
        if not os.path.isfile(self.filepath):
            self.clear_session_key()
            # r/w permissions for the file owner
            # noone else gets permissions
            os.chmod(self.filepath, 0o600)

        # load sess_key from file
        with open(self.filepath, "r") as file:
            sess_key = file.readline()
        return sess_key

    def save_session_key(self, key: str) -> None:
        with open(self.filepath, "w") as file:
            file.write(key)

    def clear_session_key(self) -> None:
        with open(self.filepath, "w") as file:
            file.write("")


class KeyctlSessionStorage(SessionStorage):
    def __init__(self):
        self.key_id: Optional[str] = None
        self.keyctl_user = 'bw_session'

    def load_session_key(self) -> str:
        if self.key_id is None:
            result, error = run_cmd(["keyctl", "request", "user", self.keyctl_user], check=False)
            if len(error) == 0:
                self.key_id = result
            elif error == 'request_key: Required key not available':
                return ''
            else:
                raise Exception(f'error in retrieving the session {error}')

        result = run_cmd(["keyctl", "print", self.key_id])
        return result

    def save_session_key(self, key: str) -> None:
        run_cmd(["keyctl", "add", "user", self.keyctl_user, key, '@s'])

    def clear_session_key(self) -> None:
        run_cmd(["keyctl", "purge", self.keyctl_user])