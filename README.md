## Disclaimer

Although I belive this program is safe, You should use this program at Your own risk.

## Installation

[Dmenu](https://tools.suckless.org/dmenu/) and [python3](https://www.python.org/) are required to run this program.
The program communicates with the user using `notify-send`, which should be available on Your system.

Of course you also need the [official CLI of Bitwarden](https://bitwarden.com/help/article/cli/). **You should
log in on Your own first**, especially if You use any form of 2-Factor Authentification (`bw login`).

You can install the package via pip using the following command:

**_Well you can't, because `python-pip` from the official Arch repo has been out-of-date for weeks now,
so I can't publish it. For now you can simply clone this repo._**

```
$ pip install dmenu_bitwarden
```

## Usage

```
$ dmenu_bitwarden
```

Password prompt should appear. After you put in Your password you can choose the account for which
you want your password. The selected password will be copied to your clipboard.

I recommend You bind this to a keyboard shortcut, e.g., `super + p`. (recommended [Hotkey daemon](https://github.com/baskerville/shkd))

## Documentation

You can find the documentation [here](https://patriktrefil.gitlab.io/dmenu-bitwarden)

Alternatively, You can use this command

```
$ cd /path/to/cloned/project
$ man ./docs/_build/man/bitwardendmenu.1
```
